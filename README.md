# pipeline_logic

This repo is put together to demonstrate GitLab with the Pipeline Logic. The code was provided to me as an example & debug purposes.

We found that particular jobs executed in master & branches but not in MRs. This led to a good bit of debugging. From this, we found:

> For every change pushed to the branch, duplicate pipelines run. One branch pipeline runs a single job (job-with-no-rules), and one merge request pipeline runs the other job (job-with-rules). Jobs with no rules default to except: merge_requests, so job-with-no-rules runs in all cases except merge requests.

[REF](https://docs.gitlab.com/ee/ci/yaml/#avoid-duplicate-pipelines)
